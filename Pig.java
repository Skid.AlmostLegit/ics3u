package com.akiadagaki;

/*
 * @ Author: Duc Anh Vu - Hestia
 * @ Last Modified: Oct 31, 2019
 * @ Description: Pig Game
 */

import java.util.*;
public class Assign7 {

    /* Method name: rollDice
     * Author: Duc Anh Vu - Hestia
     * Creation Date: Oct 29, 2019
     * Modified Date: Oct 29, 2019
     * Description: Get a random result from 1 - 6 (Dice simulation)
     * @ Parameters: None
     * @ Return Value: Integer dice, (1-6)
     * Data type: Integer
     * Dependencies: ?
     * Throws/Exceptions: N/A
     */
    public static int rollDice () {
        int dice = (int)(Math.random()*6)+1;
        return dice;
    }

    /* Method name: diceResult
     * Author: Duc Anh Vu - Hestia
     * Creation Date: Oct 28, 2019
     * Modified Date: Oct 28, 2019
     * Description: If the rollDice get 1, result = 0( not counted bcz the rules of the game)
     * @ Parameters: int x
     * @ Return Value: Integer diceResult = 0 or 1-6
     * Data type: Integer
     * Dependencies: ?
     * Throws/Exceptions: N/A
     */
    public static int diceResult (int x) {
        int diceResult = 0;
        if (x == 1)
            diceResult = 0;
        else
            diceResult = x;
        return diceResult;

    }

    /* Method name: input
     * Author: Duc Anh Vu - Hestia
     * Creation Date: Oct 29, 2019
     * Modified Date: Oct 29, 2019
     * Description: Input with Scanner
     * @ Parameters: None
     * @ Return Value: String input
     * Data type: String
     * Dependencies: ?
     * Throws/Exceptions: N/A
     */
    public static Scanner shiro = new Scanner(System.in);
    public static String input () {
        String input = shiro.nextLine();
        return input;
    }

    /* Method name: p1p2change
     * Author: Duc Anh Vu - Hestia
     * Creation Date: Oct 30, 2019
     * Modified Date: Oct 30, 2019
     * Description: Change the player assigned to p1 or p2
     * @ Parameters: String player, String p1, String p2
     * @ Return Value: String player
     * Data type: String
     * Dependencies: ?
     * Throws/Exceptions: N/A
     */
    public static String player;
    public static String p1p2change(String player, String p1, String p2) {
        if (player==p1) {
            player = p2;
        } else if (player==p2)
            player = p1;
        return player;
    }

    /* Method name: totalDice
     * Author: Duc Anh Vu - Hestia
     * Creation Date: Oct 29, 2019
     * Modified Date: Oct 29, 2019
     * Description: Calculate result of multiple rollDice() if the player not get 1
     * @ Parameters: int diceResult, String player, String p1, String p2, int totalpts
     * @ Return Value: int totalpts
     * Data type: Integer
     * Dependencies: ?
     * Throws/Exceptions: N/A
     */
    public static int totalDice(int diceResult, String p1, String p2, int totalpts) {
        if (diceResult==1) {
            return 0;
        }
        else if(diceResult>0)
            totalpts = totalpts + diceResult;
            return totalpts;
    }

    // main run
    public static void main(String[] args) {
        int p1pts=0; // indiv pts p1
        int p2pts=0; // indiv pts p2
        int solopts=0; // pts not initialized for all player assigned on
        int totalpts=0; // point combination of dice roll

        String decision = " "; // roll or hold.

        System.out.println("====================================");
        System.out.println("        Hestia Pig Game v1.0        ");
        System.out.println("====================================");
        System.out.println("\nWelcome to PIG!");

        System.out.print("\nPlayer 1, please enter your name: ");
        String p1 = input(); // name p1
        System.out.print("Player 2, please enter your name: ");
        String p2 = input(); // name p2
        System.out.println("\n"+p1+", you will go first!");
        player = p1;


        do {
            int xucxac = rollDice(); // this one for output individual dice, not a total.
            int diceResult = diceResult(xucxac); // dice res final

            totalpts = totalDice(diceResult, p1, p2, totalpts);


            // assigned pts for player
            if (player.equals(p1)) {
                solopts = p1pts; // assigned pts for p1
            } else if (player.equals(p2)) {
                solopts = p2pts; // same for p2
            }


            // show up
            System.out.println("\n" + player + " rolled a " + xucxac + "!");
            System.out.println("You have " + totalpts + " points, " + solopts + " total!\n");


            //
            char choice = ' ';
            if (totalDice(diceResult, p1, p2, totalpts) == 0) {
                System.out.println(player + " you earned " + 0 + " points and need " + (100 - solopts) + " more points to win the game!");
                player = p1p2change(player, p1, p2);

            } else if (totalDice(diceResult, p1, p2, totalpts) > 0) {
                System.out.println("Would you like to roll again (Y or N)?");
                decision = input();
                choice = decision.charAt(0);
            }

            if (choice == 'Y' || choice == 'y') {
                if (player.equals(p1)) {

                    p1pts += totalpts;

                    solopts = 0;
                    totalpts = 0;

                } else if (player.equals(p2)) {

                    p2pts += totalpts;

                    solopts = 0;
                    totalpts = 0;

                    
                }
            } else if (choice == 'N' || choice == 'n') {
                if (player.equals(p1)) {

                    p1pts += totalpts;
                    solopts += totalpts;

                    System.out.println(player + " you have " + solopts + " points and need " + (100 - solopts) + " more points to win the game!");

                    solopts = 0; // reset
                    totalpts = 0;

                    player = p1p2change(player, p1, p2);
                } else if (player.equals(p2)) {

                    p2pts += totalpts;
                    solopts += totalpts;
                    System.out.println(player + " you have " + solopts + " points and need " + (100 - solopts) + " more points to win the game!");

                    solopts = 0; // reset
                    totalpts = 0;

                    player = p1p2change(player, p1, p2);
                }
            }
            if (p1pts >= 100) {
                System.out.println("GAME OVER! " + p1 + " wins the game!");
            } else if (p2pts >= 100) {
                System.out.println("GAME OVER! " + p2 + " wins the game!");
            }
        } while((p1pts<100)||(p2pts<100));
    }

}