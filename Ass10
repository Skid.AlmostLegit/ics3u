
/*
 *@Author : Duc Anh Vu - AlmostLegit
 *@Last Modified: Dec 31, 2019
 *@Description: Sort and Search Assignment
 */



// import necessary package
import java.util.*;
import java.io.*;

public class Ass10 {
    /*
     * Method: Bubble Sort
     * Author: Duc Anh Vu
     * Creation Date: Dec 18, 2019
     * Modified Date: Dec 18, 2019
     * Description: Bubble Sort type
     * @Parameters:int[] data
     * @Return Value: N/A
     * Data Type: void
     * Dependencies: N/A
     * Throws/Exceptions: N/A
     */
    public static void bubbleSort(int data[]) {
        int tmp;
        int j=0;
        boolean sorted = false;
        while ((!sorted) && (j < data.length)){
            sorted = true;
            for (int i=0;i < data.length -1; i++)
                if (data[i] > data[i+1]) {
                    sorted = false;
                    tmp=data[i];
                    data[i]=data[i+1];
                    data[i+1]=tmp;
                } // end if
        } // end while
    } // end bubbleSort

    /*
     * Method: Insertion Sort
     * Author: Duc Anh Vu
     * Creation Date: Dec 18, 2019
     * Modified Date: Dec 18, 2019
     * Description: Insertion Sort type
     * @Parameters:int[] data
     * @Return Value: N/A
     * Data Type: void
     * Dependencies: N/A
     * Throws/Exceptions: N/A
     */
    public static void insertionSort (int data[]) {
        int tmp;
        int i, j;
        for (j=0;j<data.length;j++) {
            i=j-1;
            tmp=data[j];
            while((i>=0)&&(tmp < data[i])) {
                data[i+1]=data[i];
                i--;
            }
            data[i+1]=tmp;
        }
    } // end insertionSort

    /*
     * Method: Selection Sort
     * Author: Duc Anh Vu
     * Creation Date: Dec 18, 2019
     * Modified Date: Dec 18, 2019
     * Description: Selection Sort type
     * @Parameters:int[] intArray
     * @Return Value: N/A
     * Data Type: void
     * Dependencies: N/A
     * Throws/Exceptions: N/A
     */
    public static void selectionSort(int[] intArray) {
        for (int i = intArray.length-1; i > 0; i--) {
            int maxLoc = 0; // Location of largest item seen so far.
            for (int j = 1; j <= i; j++) {
                if (intArray[j] > intArray[maxLoc]) {
                    maxLoc = j;
                }
            }
            int temp = intArray[maxLoc]; // Swap largest item with intArray[i].
            intArray[maxLoc] = intArray[i];
            intArray[i] = temp;
        } // end of for loop
    } // end selectSort

    /*
     * Method: Random Array
     * Author: Duc Anh Vu
     * Creation Date: Dec 23, 2019
     * Modified Date: Dec 23, 2019
     * Description: Random 100000 number to an array
     * @Parameters:int[] array
     * @Return Value: N/A
     * Data Type: int[]
     * Dependencies: N/A
     * Throws/Exceptions: N/A
     */
    public static int[] randomArray (int[] array) {
        for (int i = 0;i < array.length;i++) {
            array[i] = (int)(Math.random()*100001)+1;
        }
        return array;
    } // end RandomArray

    /*
     * Method: Sequential Sort
     * Author: Duc Anh Vu
     * Creation Date: Dec 18, 2019
     * Modified Date: Dec 18, 2019
     * Description: Sequential Sort type
     * @Parameters:int[]array,int num
     * @Return Value: array
     * Data Type:void
     * Dependencies: N/A
     * Throws/Exceptions: N/A
     */
    public static void sequentialSort(int[]array,int num) {
        for(int i=0;i<array.length;i++) {
            if(array[i]==num)
                break;
        }
    } // end sequentialSort

    /*
     * Method: Binary Sort
     * Author: Duc Anh Vu
     * Creation Date: Dec 18, 2019
     * Modified Date: Dec 18, 2019
     * Description: Binary Sort type
     * @Parameters:int[] array,int num
     * @Return Value: N/A
     * Data Type: int
     * Dependencies: N/A
     * Throws/Exceptions: N/A
     */
    public static int binarySort(int[] array,int num) {
        int low = 0;
        int high = array.length - 1;
        int middle = 0;
        if(num < array[low] || num > array[high] || low > high){
            return -1;
        }
        while(low <= high){
            middle = (low + high) / 2;
            if(array[middle] > num){
                high = middle - 1;
            }else if(array[middle] < num){
                low = middle + 1;
            }else{
                return middle;
            }
        }
        return -1;
    } // end binarySort

    /*
     * Method: Get time
     * Author: Duc Anh Vu
     * Creation Date: Dec 18, 2019
     * Modified Date: Dec 18, 2019
     * Description: Get time execute
     * @Parameters: N/A
     * @Return Value: tick
     * Data Type: long
     * Dependencies: N/A
     * Throws/Exceptions: N/A
     */
    public static long getTime() {
        long tick = System.currentTimeMillis();
        return tick;
    }

    /*
     * Method: x2 array
     * Author: Duc Anh Vu
     * Creation Date: Dec 27, 2019
     * Modified Date: Dec 27, 2019
     * Description: Copy an array then paste to another with same start value
     * @Parameters:int[] array1,int[] array2
     * @Return Value: N/A
     * Data Type:int[]
     * Dependencies: N/A
     * Throws/Exceptions: N/A
     */
    public static int[] x2array(int[] array1,int[] array2) {
        for(int i=0;i<array1.length;i++) {
            array2[i]=array1[i];
        }
        return array2;
    }

    // Main
    public static void main (String[] args) {
        int[] array = new int[100000];
        randomArray(array);
        boolean infinityLoop = true;
        while (infinityLoop = true) {
            System.out.println("Menu:");
            System.out.println("What part you want to choose?");
            System.out.println("(1) Part A");
            System.out.println("(2) Part B");
            Scanner input = new Scanner(System.in);
            int answer = Integer.parseInt(input.nextLine());
            switch (answer) {
                case 1:
                    int[] data1 = new int[array.length]; // array for 1st test
                    int[] data2 = new int[array.length]; // array for out of place test
                    int totalBubbleSort=0, totalInsertionSort=0, totalSelectionSort=0, avgBubbleSort=0, avgInsertionSort=0, avgSelectionSort=0;

                    // Bubble sort test
                    System.out.println("Bubble sort test");
                    for(int i=1;i<4;i++) {
                        x2array(array,data1);
                        int time = 0;
                        long beforeSort = getTime();
                        bubbleSort(data1);
                        long afterSort = getTime(); //afterSort time in milliseconds
                        time = (int)(afterSort - beforeSort); //difference b/w after and before
                        System.out.println("Trial " + i + ": " + time);
                        totalBubbleSort += time;
                    }

                    // Insertion sort test
                    System.out.println("Insertion sort test");
                    for(int i=1;i<4;i++) {
                        x2array(array,data1);
                        int time = 0;
                        long beforeSort = getTime();
                        insertionSort(data1);
                        long afterSort = getTime(); //afterSort time in milliseconds
                        time = (int)(afterSort - beforeSort); //difference b/w after and before
                        System.out.println("Trial " + i + ": " + time);
                        totalInsertionSort += time;
                    }

                    // Selection sort test
                    System.out.println("Selection sort test");
                    for(int i=1;i<4;i++) {
                        x2array(array,data1);
                        int time = 0;
                        long beforeSort = getTime();
                        selectionSort(data1);
                        long afterSort = getTime(); //afterSort time in milliseconds
                        time = (int)(afterSort - beforeSort); //difference b/w after and before
                        System.out.println("Trial " + i + ": " + time);
                        totalSelectionSort += time;
                    }
                    
                    // Average calculation
                    avgBubbleSort = totalBubbleSort/3;
                    avgInsertionSort = totalInsertionSort/3;
                    avgSelectionSort = totalSelectionSort/3;
                    System.out.println("====================================");
                    System.out.println("    Random Arrays (3 trials each)   ");
                    System.out.println("====================================");
                    System.out.println("Bubble sort takes on average "+avgBubbleSort+" milliseconds");
                    System.out.println("Insertion sort takes on average "+avgInsertionSort+" milliseconds");
                    System.out.println("Selection sort takes on average "+avgSelectionSort+" milliseconds\n");


                    // Start out of place sort
                    // Bubble sort test
                    System.out.println("Bubble sort test");
                    for(int i=1;i<4;i++) {
                        x2array(array,data2);
                        data2[0]=50;
                        int time = 0;
                        long beforeSort = getTime();
                        bubbleSort(data1);
                        long afterSort = getTime(); //afterSort time in milliseconds
                        time = (int)(afterSort - beforeSort); //difference b/w after and before
                        System.out.println("Trial " + i + ": " + time);
                        totalBubbleSort += time;
                    }

                    // Insertion sort test
                    System.out.println("Insertion sort test");
                    for(int i=1;i<4;i++) {
                        x2array(array,data1);
                        data2[0]=50;
                        int time = 0;
                        long beforeSort = getTime();
                        insertionSort(data1);
                        long afterSort = getTime(); //afterSort time in milliseconds
                        time = (int)(afterSort - beforeSort); //difference b/w after and before
                        System.out.println("Trial " + i + ": " + time);
                        totalInsertionSort += time;
                    }

                    // Selection sort test
                    System.out.println("Selection sort test");
                    for(int i=1;i<4;i++) {
                        x2array(array,data1);
                        data2[0]=50;
                        int time = 0;
                        long beforeSort = getTime();
                        selectionSort(data1);
                        long afterSort = getTime(); //afterSort time in milliseconds
                        time = (int)(afterSort - beforeSort); //difference b/w after and before
                        System.out.println("Trial " + i + ": " + time);
                        totalSelectionSort += time;
                    }

                    avgBubbleSort = totalBubbleSort/3;
                    avgInsertionSort = totalInsertionSort/3;
                    avgSelectionSort = totalSelectionSort/3;
                    System.out.println("====================================");
                    System.out.println("      One Element out of Place      ");
                    System.out.println("====================================");
                    System.out.println("Bubble sort takes on average "+avgBubbleSort+" milliseconds");
                    System.out.println("Insertion sort takes on average "+avgInsertionSort+" milliseconds");
                    System.out.println("Selection sort takes on average "+avgSelectionSort+" milliseconds\n");

                    // Final report
                    System.out.println("When sorting, an SELECTION SORT is usually best, as it becomes more efficient in comparison to other sorts as");
                    System.out.println("the size of the data set increases. INSERTION SORT was the second fastest sort for sorting a completely random");
                    System.out.println("data set, yet when only one element was out of place, it was extremely inefficient. The BUBBLE SORT took");
                    System.out.println("the longest for sorting a random data set that had yet to be sorted,but was much more efficient than");
                    System.out.println("the SELECTION SORT for sorting a data set with only one element out of place.\n");
                    break; // end case 1

                case 2:
                    // Best case

                    int totalSequentialSort = 0, totalBinarySort=0, avgSequentialSort=0, avgBinarySort=0;

                    // Sequential sort
                    int[] data3 = new int[array.length];
                    x2array(array,data3);
                    int nanotime1 = 0;
                    long beforeSort1 = System.nanoTime();
                    sequentialSort(data3,data3[0]);
                    long afterSort1 = System.nanoTime();//afterSort time in Nano Seconds
                    nanotime1 = (int)(afterSort1 - beforeSort1);//difference b/w after and before
                    //Binary
                    x2array(array,data3);
                    int nanotime2 = 0;
                    long beforeSort2 = System.nanoTime();
                    binarySort(data3,data3[0]);
                    long afterSort2 = System.nanoTime();//afterSort time in Nano Seconds
                    nanotime1 = (int)(afterSort2 - beforeSort2);//difference b/w after and before
                    System.out.println("====================================");
                    System.out.println("             Best case              ");
                    System.out.println("====================================");
                    System.out.println("Sequential search takes "+nanotime1+" nanoseconds");
                    System.out.println("Binary search takes "+nanotime2+" nanoseconds");

                    // Worst case
                    nanotime1=0;
                    nanotime2=0;
                    beforeSort1 = System.nanoTime();
                    sequentialSort(data3,-11);
                    afterSort1 = System.nanoTime();//afterSort time in Nano Seconds
                    nanotime1 = (int)(afterSort1 - beforeSort1);//difference b/w after and before
                    beforeSort2 = System.nanoTime();
                    binarySort(data3,-11);
                    afterSort2 = System.nanoTime(); //afterSort time in Nano Seconds
                    nanotime2 = (int)(afterSort2 - beforeSort2);//difference b/w after and before
                    System.out.println("**********Worst Case**********");
                    System.out.println("Sequential search takes "+nanotime1+" nanoeconds");
                    System.out.println("Binary search takes "+nanotime2+" nanoseconds");

                    // Average case
                    int[] data4 = new int[array.length];
                    for(int i=1;i<4;i++) {
                        randomArray(data4);
                        int time = 0;
                        long beforeSort = System.nanoTime();
                        sequentialSort(data4,data4[(int)Math.random()*100000+1]);
                        long afterSort = System.nanoTime(); //afterSort time in Nano Seconds
                        time = (int)(afterSort - beforeSort); //difference b/w after and before
                        totalSequentialSort += time;
                    }
                    for(int i=1;i<4;i++) {
                        randomArray(data4);
                        int time = 0;
                        long beforeSort = System.nanoTime();
                        binarySort(data4,data4[(int)Math.random()*100000+1]);
                        long afterSort = System.nanoTime(); //afterSort time in Nano Seconds
                        time = (int)(afterSort - beforeSort); //difference b/w after and before
                        totalBinarySort += time;
                    }
                    avgSequentialSort=totalSequentialSort/3;
                    avgBinarySort=totalBinarySort/3;
                    System.out.println("**********Average Case**********");
                    System.out.println("Sequential search takes "+avgSequentialSort+" nanoseconds");
                    System.out.println("Binary search takes "+avgBinarySort+" nanoseconds");
                    System.out.println("***************Report***************");
                    System.out.println("A BINARY search is often more efficient than a SEQUENTIAL search. In a best case scenario, a BINARY search and a Sequential search will");
                    System.out.println("take the same amount, as the key is found in the 0 index searched. In a worst case scenario, where the");
                    System.out.println("key is no found within the array, a Sequential search will take longer because it must check every element in the");
                    System.out.println("array before determining that the key is not in the array. In an average array, the Binary search will be faster");
                    System.out.println("than the SEQUENTIAL search, growing more efficient in comparison as the size of the data set increases.\n");
                    break;
            } input.close();
        }
    }
}
