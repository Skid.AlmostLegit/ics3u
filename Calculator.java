/*
 *@Author: Duc Anh Vu - Hestia
 *@Last Modified: 9:53PM - Nov 11,2019
 *@Description: Calculator with swing for ui
 */

package maytinh;

/*
 * Import necessary
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class Calculator implements ActionListener {

    /*
     * Variables necessary
     */
    JFrame khung;
    JTextField khungText;
    JButton n1,n2,n3,n4,n5,n6,n7,n8,n9,n0,nCong,nTru,nNhan,nChia,nBang,nXoa,nAC,nCham;
    public static double so1=0, so2=0, ketqua=0;
    public static int congtrunhanchia=0;

    /*
     * UI
     */
    Calculator() {
        khung = new JFrame("Hestia Calculator v1.0");
        khungText = new JTextField();
        khungText.setEditable(false);
        n1 = new JButton("1");
        n2 = new JButton("2");
        n3 = new JButton("3");
        n4 = new JButton("4");
        n5 = new JButton("5");
        n6 = new JButton("6");
        n7 = new JButton("7");
        n8 = new JButton("8");
        n9 = new JButton("9");
        n0 = new JButton("0");
        nCong = new JButton("+");
        nTru = new JButton("-");
        nNhan = new JButton("*");
        nChia = new JButton("/");
        nBang = new JButton("=");
        nXoa = new JButton("Del");
        nAC = new JButton("AC");
        nCham = new JButton(".");

        khungText.setBounds(30, 40, 280, 30);
        n1.setBounds(40, 240, 50, 40);
        n2.setBounds(110, 240, 50, 40);
        n3.setBounds(180, 240, 50, 40);
        n4.setBounds(40, 170, 50, 40);
        n5.setBounds(110, 170, 50, 40);
        n6.setBounds(180, 170, 50, 40);
        n7.setBounds(40, 100, 50, 40);
        n8.setBounds(110, 100, 50, 40);
        n9.setBounds(180, 100, 50, 40);
        n0.setBounds(110, 310, 50, 40);
        nChia.setBounds(250, 100, 50, 40);
        nNhan.setBounds(250, 170, 50, 40);
        nCong.setBounds(250, 310, 50, 40);
        nTru.setBounds(250, 240, 50, 40);
        nBang.setBounds(180,310,50,40);
        nXoa.setBounds(60, 380, 100, 40);
        nAC.setBounds(180, 380, 100, 40);
        nCham.setBounds(40, 310, 50, 40);

        khung.add(khungText);
        khung.add(n1);
        khung.add(n2);
        khung.add(n3);
        khung.add(n4);
        khung.add(n5);
        khung.add(n6);
        khung.add(n7);
        khung.add(n8);
        khung.add(n9);
        khung.add(n0);
        khung.add(nChia);
        khung.add(nNhan);
        khung.add(nCong);
        khung.add(nTru);
        khung.add(nBang);
        khung.add(nXoa);
        khung.add(nAC);
        khung.add(nCham);

        khung.setLayout(null);
        khung.setVisible(true);
        khung.setSize(350, 500);
        khung.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        khung.setResizable(false);

        n1.addActionListener(this);
        n2.addActionListener(this);
        n3.addActionListener(this);
        n4.addActionListener(this);
        n5.addActionListener(this);
        n6.addActionListener(this);
        n7.addActionListener(this);
        n8.addActionListener(this);
        n9.addActionListener(this);
        n0.addActionListener(this);
        nBang.addActionListener(this);
        nNhan.addActionListener(this);
        nChia.addActionListener(this);
        nCong.addActionListener(this);
        nTru.addActionListener(this);
        nXoa.addActionListener(this);
        nAC.addActionListener(this);
        nCham.addActionListener(this);
    }
    public void actionPerformed(ActionEvent hd) {
        if (hd.getSource() == n1)
            khungText.setText(khungText.getText().concat("1"));

        if (hd.getSource() == n2)
            khungText.setText(khungText.getText().concat("2"));

        if (hd.getSource() == n3)
            khungText.setText(khungText.getText().concat("3"));

        if (hd.getSource() == n4)
            khungText.setText(khungText.getText().concat("4"));

        if (hd.getSource() == n5)
            khungText.setText(khungText.getText().concat("5"));

        if (hd.getSource() == n6)
            khungText.setText(khungText.getText().concat("6"));

        if (hd.getSource() == n7)
            khungText.setText(khungText.getText().concat("7"));

        if (hd.getSource() == n8)
            khungText.setText(khungText.getText().concat("8"));

        if (hd.getSource() == n9)
            khungText.setText(khungText.getText().concat("9"));

        if (hd.getSource() == n0)
            khungText.setText(khungText.getText().concat("0"));

        if(hd.getSource()==nCham)
            khungText.setText(khungText.getText().concat("."));

        if (hd.getSource() == nCong) {
            so1 = Double.parseDouble(khungText.getText());
            congtrunhanchia = 1;
            khungText.setText("");
        }

        if (hd.getSource() == nTru) {
            so1 = Double.parseDouble(khungText.getText());
            congtrunhanchia = 2;
            khungText.setText("");
        }

        if (hd.getSource() == nNhan) {
            so1 = Double.parseDouble(khungText.getText());
            congtrunhanchia = 3;
            khungText.setText("");
        }

        if (hd.getSource() == nChia) {
            so1 = Double.parseDouble(khungText.getText());
            congtrunhanchia = 4;
            khungText.setText("");
        }

        if (hd.getSource() == nBang) {
            so2 = Double.parseDouble(khungText.getText());
            switch (congtrunhanchia) {
                case 1:
                    ketqua = so1 + so2;
                    break;

                case 2:
                    ketqua = so1 - so2;
                    break;

                case 3:
                    ketqua = so1 * so2;
                    break;

                case 4:
                    ketqua = so1 / so2;
                    break;

                default:
                    ketqua = 0;
            }

            khungText.setText("" + ketqua);
        }

        if (hd.getSource() == nAC)
            khungText.setText("");

        if (hd.getSource() == nXoa) {
            String s = khungText.getText();
            khungText.setText("");
            for (int i = 0; i < s.length() - 1; i++)
                khungText.setText(khungText.getText() + s.charAt(i));
        }
    }

    // main
    public static void main (String[]args) {
        new Calculator();
    }
}
